import React, { Component } from "react";
import { connect } from "react-redux";

class ReduxMini extends Component {
  render() {
    return (
      <div>
        {" "}
        <button
          onClick={() => {
            this.props.hendLeGiamSoLuong();
          }}
          className="btn btn-success "
        >
          -
        </button>
        <strong className="mx-3">{this.props.number}</strong>
        <button
          onClick={this.props.hendLeTangSoLuong}
          className="btn btn-danger "
        >
          +
        </button>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    number: state.numberReducer.number,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    hendLeTangSoLuong: () => {
      let action = {
        type: "SO_LUONG_TANG",
        payload: 5,
      };
      dispatch(action);
    },
    hendLeGiamSoLuong: () => {
      dispatch({
        type: "SO_LUONG_GIAM",
        payload: 5,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ReduxMini);
