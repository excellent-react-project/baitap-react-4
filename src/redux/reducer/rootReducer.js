import { combineReducers } from "redux";
import { numberReducer } from "./numberReducer";

export const rootReducer_ReducerMini = combineReducers({
  numberReducer: numberReducer,
});
