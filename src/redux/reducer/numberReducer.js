let innitialValue = { number: 1 };

export const numberReducer = (state = innitialValue, action) => {
  switch (action.type) {
    case "SO_LUONG_TANG": {
      state.number = state.number + action.payload;
      return { ...state };
    }
    case "SO_LUONG_GIAM": {
      state.number -= action.payload;
      return { ...state };
    }

    default: {
      return state;
    }
  }
};
